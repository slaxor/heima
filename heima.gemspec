
lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "heima/version"

Gem::Specification.new do |spec|
  spec.name          = "heima"
  spec.version       = Heima::VERSION
  spec.authors       = ["Steffen Leistner"]
  spec.email         = ["sleistner@gmail.com"]

  spec.summary       = "Simple network benchmarking"
  spec.description   = "Print the average response time for a given url"
  spec.homepage      = "https://gitlab.com/sleistner/heima"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "thor", "~> 0.20.0"
  spec.add_dependency "typhoeus", "~> 1.3"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rubocop", "~> 0.55.0"
end
