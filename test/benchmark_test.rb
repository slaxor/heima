require "test_helper"
require "typhoeus"
require "net/http"

describe Heima::Benchmark do
  def subject(*args)
    Heima::Benchmark.new(*args)
  end

  before do
    Typhoeus.stub("example.com").and_return(Typhoeus::Response.new(code: 200))
    Typhoeus.stub("example").and_return(Typhoeus::Response.new(code: 0))
  end

  after do
    Typhoeus::Expectation.clear
  end

  describe "initialize" do
    describe "uri param validation" do
      describe "valid uri supplied" do
        it "creates an instance" do
          assert_kind_of(Heima::Benchmark, subject(uri: "example.com"))
        end
      end

      describe "invalid uri supplied" do
        it "raises an error" do
          assert_raises(URI::InvalidURIError) do
            subject(uri: "example")
          end
        end
      end

      describe "nil uri supplied" do
        it "raises an error" do
          assert_raises(URI::InvalidURIError) do
            subject(uri: nil)
          end
        end
      end
    end

    describe "duration" do
      it "defaults to 60 seconds" do
        assert_equal(60, subject(uri: "example.com").duration)
      end
      it "rejects invalid values" do
        assert_raises(ArgumentError, "duration must be be given in seconds") do
          subject(uri: "example.com", duration: 0)
        end
      end
    end

    describe "interval" do
      it "defaults to 10 seconds" do
        assert_equal(10, subject(uri: "example.com").interval)
      end
      it "rejects invalid values" do
        assert_raises(ArgumentError, "interval must be be given in seconds") do
          subject(uri: "example.com", duration: 1, interval: 2)
        end
      end

      describe "value lower than duration" do
        it "raises an error" do
          assert_raises(ArgumentError, "interval must be lower than or equal duration") do
            subject(uri: "example.com", duration: 5, interval: 7)
          end
        end
      end
    end
  end

  describe "run" do
    before do
      Typhoeus.stub("run.example.com").and_return(Typhoeus::Response.new(code: 200, total_time: 0.31288))
      @subject = subject(uri: "run.example.com", duration: 2, interval: 1)
    end

    it "returns a result object" do
      assert_kind_of(Heima::BenchmarkResult, @subject.run)
    end

    it "yields a result object" do
      result = nil
      @subject.run { |r| result = r }
      assert_kind_of(Heima::BenchmarkResult, result)
    end

    describe "avg calculation" do
      it "provides an avg response time" do
        assert_equal(312.88, @subject.run.avg)
      end
    end

    describe "expected number of requests" do
      it "probes the uri exactly 2 times" do
        assert_equal(2, @subject.run.requests.size)
      end
    end

    describe "expected interval" do
      before do
        @subject = subject(uri: "run.example.com", duration: 6, interval: 2)
      end

      it "probes the uri exactly every 2 seconds" do
        requests = @subject.run.requests.map(&:start_time)
        assert_equal(requests[1].to_s, (requests[0] + 2).to_s)
        assert_equal(requests[2].to_s, (requests[0] + 4).to_s)
      end
    end
  end
end
