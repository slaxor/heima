require "net/http"
require "typhoeus"
require "heima/benchmark_result"

module Heima
  class Benchmark
    attr_reader :uri, :duration, :interval

    def initialize(uri:, duration: 60, interval: 10)
      @uri = URI.parse(uri).to_s
      raise URI::InvalidURIError unless self.class.probe_uri(@uri)

      @duration = duration
      raise ArgumentError, "duration must be be given in seconds" unless @duration&.positive?

      @interval = interval
      raise ArgumentError, "interval must be be given in seconds" unless @interval&.positive?
      raise ArgumentError, "interval must be lower than or equal duration" if @interval > @duration
    end

    def run
      executions = @duration / @interval
      result = execute(@uri, @interval, executions)

      if block_given?
        yield result
      else
        result
      end
    end

    def self.probe_uri(uri)
      Typhoeus.head(uri, followlocation: true).code.positive?
    end

    private

    def execute(uri, interval, executions)
      threads = Array.new(executions) { |pos| create_executable(uri, pos, interval) }.map(&:join)
      avg = threads.map(&:value).map(&:total_time).sum / executions

      BenchmarkResult.new(
        avg: avg.round(2),
        requests: threads.map(&:value)
      )
    end

    def create_executable(uri, pos, interval)
      Thread.new do
        sleep interval * pos if pos.positive?
        Probe.new(
          pos + 1,
          Time.now,
          Typhoeus.get(uri, followlocation: true).total_time * 1000
        )
      end
    end
  end

  class Probe
    attr_reader :execution, :start_time, :total_time

    def initialize(execution, start_time, total_time)
      @execution = execution
      @start_time = start_time
      @total_time = total_time
    end
  end
end
